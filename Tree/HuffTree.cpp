#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include "HuffTree.h"

bool cmpr(Node *u1, Node *u2) {
	return u1->key < u2->key;
}

HuffTree::~HuffTree() {
	for (auto el : v)
		delete el;
	delete root;
}

int* HuffTree::periodicity(char const* namef) {
	const int alen = 256;
	if (a)
		delete[] a;
	a = new int[alen];
	for (int i = 0; i < alen; ++i)
		a[i] = 0;

	std::ifstream f(namef);
	if (!f)
		throw std::runtime_error("error opening file");

	char r;
	while (f.get(r)) {
		a[(unsigned int)r]++;
		++m;
	}

	f.close();

	return a;
}

std::string HuffTree::create_list() {
	for (int i = 0; i < 256; ++i)
		if (a[i]) {
			Node *p = new Node(a[i], char(i));
			v.push_back(p);
		}

	std::sort(v.begin(), v.end(), cmpr);

	std::string res = "";
	for (std::vector<Node*>::iterator pv = v.begin(); pv != v.end(); ++pv) {
		Node* q = *pv;

		for (int i = 0; i < 256; i++)
			if ((q->s)[i])
				res += std::to_string(i) + ' ';

		res.pop_back(); // remove last char
	}

	return res;
}

void HuffTree::create_tree() {
	Node* p;
	int d = 2;
	std::vector<Node*>::iterator pv = v.begin();

	int k = v.size();

	while (k-- > 1) {
		Node* p1 = *pv;
		Node* p2 = *(pv + 1);

		p = new Node(p1->key + p2->key, 0, p1, p2);

		p->s = p1->s | p2->s;
		v.push_back(p);

		pv = v.begin();
		pv += d;
		d = d + 2;

		std::sort(pv, v.end(), cmpr);
	}

	root = p;
}

std::string HuffTree::code(char const* namef1, char const* namef2) {
	std::ifstream f(namef1);
	std::ofstream ff(namef2);

	Node* p = root;
	int L = 8;
	n = 0;
	char r;
	unsigned char tmp = 0;
	std::string res = "";

	while (f.get(r)) {
		p = root;
		while (p->left) {
			if ((p->left->s)[(unsigned char)r]) {
				res += '0';
				tmp = tmp << 1;
				p = p->left;
				L--; n++;
			} else {
				res += '1';
				p = p->right;
				tmp = (tmp << 1) | 1;
				L--; n++;
			}

			if (!L) {
				ff << tmp;
				L = 8;
			}
		}
	}
	if (L) {
		while (L) {
			tmp = tmp << 1;
			L--;
		}
		ff << tmp;
	}

	return res;
}

std::string HuffTree::decode(char const* namef1) {
	std::ifstream f(namef1);
	Node* p = root;

	int k = n;
	int i, L = 7, flg = 0;
	char r, tm, tmp = 1;

	std::string res = "";

	f.get(r);
	while (f) {
		if (flg == 1)
			f.get(r);
		flg = 0;

		while (p->left && L >= 0) {
			if (r & (tmp << L))
				p = p->right;
			else
				p = p->left;

			--L;
			--k;
			if (k < 0)
				return res;
		}

		if (L == -1) {
			flg = 1;
			L = 7; }
		if (!(p->left)) {
			for (i = 0; i < 256 && !(p->s)[i]; ++i);
			tm = i;
			res += (char)tm;

			p = root;
		}
	}

	return res;
}

float HuffTree::compression_ratio() {
	if (!n || !m)
		return 0;

	return float(n) / (m * 8) * 100;
}
