#pragma once

#include <map>
#include "AskDialog.h"
#include "Node.h"
#include "FormTree.h"
#include "BinaryTree.h"
#include "FindTree.h"
#include "HuffTree.h"

char* and_SysStringToChar(System::String^ string) {
	return (char*)(void*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(string);
}

namespace Tree {
	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form {
		typedef int LogLevel;
		ref struct LogLevels {
			LogLevel verbose = 1 << 2;
			LogLevel info = 1 << 1;
			LogLevel error = 1;
		};

		FormTree* formtree;
		BinaryTree* binarytree;
		FindTree* findtree;
		HuffTree* hufftree;

		LogLevel logLevel;

	private: System::Windows::Forms::TextBox^  logBox;

	private: System::Windows::Forms::TableLayoutPanel^  tableLayoutPanel1;
	private: System::Windows::Forms::ToolStripMenuItem^  heightToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  numberOfNodesToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  byLevelToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  wholeTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  idealToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  balanceToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  sumToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  huffmanToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  findTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  replaceWithArrayToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  printEvenToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  minToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  maxToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  huffmanTreeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  codeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  decodeToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog;
	private: System::Windows::Forms::ToolStripMenuItem^  compressionRateToolStripMenuItem;


			 LogLevels logLevels;

	public:
		MyForm(void) {
			InitializeComponent();
			logLevel = logLevels.verbose | logLevels.info | logLevels.error;
			formtree = new FormTree();
			Verbose("Initialized");
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm() {
			if (components) {
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected:
	private: System::Windows::Forms::ToolStripMenuItem^  buildToolStripMenuItem;


	private: System::Windows::Forms::ToolStripMenuItem^  binaryToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  findToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  actionsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  drawToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  findToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  addToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  deleteToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  checkToolStripMenuItem;

	private: System::Windows::Forms::Panel^  drawPanel;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->buildToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->binaryToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->findToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->huffmanToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->actionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->drawToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->wholeTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->byLevelToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->findToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->addToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->deleteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->findTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->replaceWithArrayToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->huffmanTreeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->codeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->decodeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->compressionRateToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->checkToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->heightToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->numberOfNodesToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->idealToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->balanceToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->sumToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->printEvenToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->minToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->maxToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->drawPanel = (gcnew System::Windows::Forms::Panel());
			this->logBox = (gcnew System::Windows::Forms::TextBox());
			this->tableLayoutPanel1 = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->openFileDialog = (gcnew System::Windows::Forms::OpenFileDialog());
			this->menuStrip1->SuspendLayout();
			this->tableLayoutPanel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->buildToolStripMenuItem,
					this->actionsToolStripMenuItem, this->checkToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(491, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// buildToolStripMenuItem
			// 
			this->buildToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->binaryToolStripMenuItem,
					this->findToolStripMenuItem, this->huffmanToolStripMenuItem
			});
			this->buildToolStripMenuItem->Name = L"buildToolStripMenuItem";
			this->buildToolStripMenuItem->Size = System::Drawing::Size(46, 20);
			this->buildToolStripMenuItem->Text = L"Build";
			// 
			// binaryToolStripMenuItem
			// 
			this->binaryToolStripMenuItem->Name = L"binaryToolStripMenuItem";
			this->binaryToolStripMenuItem->Size = System::Drawing::Size(122, 22);
			this->binaryToolStripMenuItem->Text = L"Binary";
			this->binaryToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::binaryToolStripMenuItem_Click);
			// 
			// findToolStripMenuItem
			// 
			this->findToolStripMenuItem->Name = L"findToolStripMenuItem";
			this->findToolStripMenuItem->Size = System::Drawing::Size(122, 22);
			this->findToolStripMenuItem->Text = L"Find";
			this->findToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::findToolStripMenuItem_Click);
			// 
			// huffmanToolStripMenuItem
			// 
			this->huffmanToolStripMenuItem->Name = L"huffmanToolStripMenuItem";
			this->huffmanToolStripMenuItem->Size = System::Drawing::Size(122, 22);
			this->huffmanToolStripMenuItem->Text = L"Huffman";
			this->huffmanToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::huffmanToolStripMenuItem_Click);
			// 
			// actionsToolStripMenuItem
			// 
			this->actionsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {
				this->drawToolStripMenuItem,
					this->findToolStripMenuItem1, this->addToolStripMenuItem, this->deleteToolStripMenuItem, this->findTreeToolStripMenuItem, this->huffmanTreeToolStripMenuItem
			});
			this->actionsToolStripMenuItem->Name = L"actionsToolStripMenuItem";
			this->actionsToolStripMenuItem->Size = System::Drawing::Size(59, 20);
			this->actionsToolStripMenuItem->Text = L"Actions";
			// 
			// drawToolStripMenuItem
			// 
			this->drawToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->wholeTreeToolStripMenuItem,
					this->byLevelToolStripMenuItem
			});
			this->drawToolStripMenuItem->Name = L"drawToolStripMenuItem";
			this->drawToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->drawToolStripMenuItem->Text = L"Draw";
			this->drawToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::drawToolStripMenuItem_Click);
			// 
			// wholeTreeToolStripMenuItem
			// 
			this->wholeTreeToolStripMenuItem->Name = L"wholeTreeToolStripMenuItem";
			this->wholeTreeToolStripMenuItem->Size = System::Drawing::Size(131, 22);
			this->wholeTreeToolStripMenuItem->Text = L"Whole tree";
			this->wholeTreeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::wholeTreeToolStripMenuItem_Click);
			// 
			// byLevelToolStripMenuItem
			// 
			this->byLevelToolStripMenuItem->Name = L"byLevelToolStripMenuItem";
			this->byLevelToolStripMenuItem->Size = System::Drawing::Size(131, 22);
			this->byLevelToolStripMenuItem->Text = L"By level";
			this->byLevelToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::byLevelToolStripMenuItem_Click);
			// 
			// findToolStripMenuItem1
			// 
			this->findToolStripMenuItem1->Name = L"findToolStripMenuItem1";
			this->findToolStripMenuItem1->Size = System::Drawing::Size(152, 22);
			this->findToolStripMenuItem1->Text = L"Find";
			this->findToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyForm::findToolStripMenuItem1_Click);
			// 
			// addToolStripMenuItem
			// 
			this->addToolStripMenuItem->Name = L"addToolStripMenuItem";
			this->addToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->addToolStripMenuItem->Text = L"Add";
			this->addToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::addToolStripMenuItem_Click);
			// 
			// deleteToolStripMenuItem
			// 
			this->deleteToolStripMenuItem->Name = L"deleteToolStripMenuItem";
			this->deleteToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->deleteToolStripMenuItem->Text = L"Delete";
			this->deleteToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::deleteToolStripMenuItem_Click);
			// 
			// findTreeToolStripMenuItem
			// 
			this->findTreeToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->replaceWithArrayToolStripMenuItem });
			this->findTreeToolStripMenuItem->Name = L"findTreeToolStripMenuItem";
			this->findTreeToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->findTreeToolStripMenuItem->Text = L"Find tree";
			// 
			// replaceWithArrayToolStripMenuItem
			// 
			this->replaceWithArrayToolStripMenuItem->Name = L"replaceWithArrayToolStripMenuItem";
			this->replaceWithArrayToolStripMenuItem->Size = System::Drawing::Size(170, 22);
			this->replaceWithArrayToolStripMenuItem->Text = L"Replace with array";
			this->replaceWithArrayToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::replaceWithArrayToolStripMenuItem_Click);
			// 
			// huffmanTreeToolStripMenuItem
			// 
			this->huffmanTreeToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->codeToolStripMenuItem,
					this->decodeToolStripMenuItem, this->compressionRateToolStripMenuItem
			});
			this->huffmanTreeToolStripMenuItem->Name = L"huffmanTreeToolStripMenuItem";
			this->huffmanTreeToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->huffmanTreeToolStripMenuItem->Text = L"Huffman tree";
			// 
			// codeToolStripMenuItem
			// 
			this->codeToolStripMenuItem->Name = L"codeToolStripMenuItem";
			this->codeToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->codeToolStripMenuItem->Text = L"Code";
			this->codeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::codeToolStripMenuItem_Click);
			// 
			// decodeToolStripMenuItem
			// 
			this->decodeToolStripMenuItem->Name = L"decodeToolStripMenuItem";
			this->decodeToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->decodeToolStripMenuItem->Text = L"Decode";
			this->decodeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::decodeToolStripMenuItem_Click);
			// 
			// compressionRateToolStripMenuItem
			// 
			this->compressionRateToolStripMenuItem->Name = L"compressionRateToolStripMenuItem";
			this->compressionRateToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->compressionRateToolStripMenuItem->Text = L"Compression rate";
			this->compressionRateToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::compressionRateToolStripMenuItem_Click);
			// 
			// checkToolStripMenuItem
			// 
			this->checkToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {
				this->heightToolStripMenuItem,
					this->numberOfNodesToolStripMenuItem, this->idealToolStripMenuItem, this->balanceToolStripMenuItem, this->sumToolStripMenuItem,
					this->printEvenToolStripMenuItem, this->minToolStripMenuItem, this->maxToolStripMenuItem
			});
			this->checkToolStripMenuItem->Name = L"checkToolStripMenuItem";
			this->checkToolStripMenuItem->Size = System::Drawing::Size(52, 20);
			this->checkToolStripMenuItem->Text = L"Check";
			// 
			// heightToolStripMenuItem
			// 
			this->heightToolStripMenuItem->Name = L"heightToolStripMenuItem";
			this->heightToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->heightToolStripMenuItem->Text = L"Height";
			this->heightToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::heightToolStripMenuItem_Click);
			// 
			// numberOfNodesToolStripMenuItem
			// 
			this->numberOfNodesToolStripMenuItem->Name = L"numberOfNodesToolStripMenuItem";
			this->numberOfNodesToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->numberOfNodesToolStripMenuItem->Text = L"Number of nodes";
			this->numberOfNodesToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::numberOfNodesToolStripMenuItem_Click);
			// 
			// idealToolStripMenuItem
			// 
			this->idealToolStripMenuItem->Name = L"idealToolStripMenuItem";
			this->idealToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->idealToolStripMenuItem->Text = L"Ideal";
			this->idealToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::idealToolStripMenuItem_Click);
			// 
			// balanceToolStripMenuItem
			// 
			this->balanceToolStripMenuItem->Name = L"balanceToolStripMenuItem";
			this->balanceToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->balanceToolStripMenuItem->Text = L"Balance";
			this->balanceToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::balanceToolStripMenuItem_Click);
			// 
			// sumToolStripMenuItem
			// 
			this->sumToolStripMenuItem->Name = L"sumToolStripMenuItem";
			this->sumToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->sumToolStripMenuItem->Text = L"Sum";
			this->sumToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::sumToolStripMenuItem_Click);
			// 
			// printEvenToolStripMenuItem
			// 
			this->printEvenToolStripMenuItem->Name = L"printEvenToolStripMenuItem";
			this->printEvenToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->printEvenToolStripMenuItem->Text = L"Print even";
			this->printEvenToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::printEvenToolStripMenuItem_Click);
			// 
			// minToolStripMenuItem
			// 
			this->minToolStripMenuItem->Name = L"minToolStripMenuItem";
			this->minToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->minToolStripMenuItem->Text = L"Min";
			this->minToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::minToolStripMenuItem_Click);
			// 
			// maxToolStripMenuItem
			// 
			this->maxToolStripMenuItem->Name = L"maxToolStripMenuItem";
			this->maxToolStripMenuItem->Size = System::Drawing::Size(167, 22);
			this->maxToolStripMenuItem->Text = L"Max";
			this->maxToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::maxToolStripMenuItem_Click);
			// 
			// drawPanel
			// 
			this->drawPanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->drawPanel->Location = System::Drawing::Point(3, 3);
			this->drawPanel->Name = L"drawPanel";
			this->drawPanel->Size = System::Drawing::Size(485, 291);
			this->drawPanel->TabIndex = 2;
			// 
			// logBox
			// 
			this->logBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->logBox->Location = System::Drawing::Point(3, 300);
			this->logBox->Multiline = true;
			this->logBox->Name = L"logBox";
			this->logBox->Size = System::Drawing::Size(485, 94);
			this->logBox->TabIndex = 3;
			// 
			// tableLayoutPanel1
			// 
			this->tableLayoutPanel1->ColumnCount = 1;
			this->tableLayoutPanel1->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->tableLayoutPanel1->Controls->Add(this->logBox, 0, 1);
			this->tableLayoutPanel1->Controls->Add(this->drawPanel, 0, 0);
			this->tableLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tableLayoutPanel1->Location = System::Drawing::Point(0, 24);
			this->tableLayoutPanel1->Name = L"tableLayoutPanel1";
			this->tableLayoutPanel1->RowCount = 2;
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 75)));
			this->tableLayoutPanel1->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 25)));
			this->tableLayoutPanel1->Size = System::Drawing::Size(491, 397);
			this->tableLayoutPanel1->TabIndex = 4;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(491, 421);
			this->Controls->Add(this->tableLayoutPanel1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MyForm";
			this->Text = L"Tree";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->tableLayoutPanel1->ResumeLayout(false);
			this->tableLayoutPanel1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	void DeleteTree() {
		if (binarytree) {
			delete binarytree;
			binarytree = nullptr;
			findtree = nullptr;
			hufftree = nullptr;
		}
	}

	void SetTree(std::string type) {
		std::map<std::string, int> trees;

		trees["binary"] = 0;
		trees["find"] = 1;
		trees["huff"] = 2;

		DeleteTree();
		switch (trees[type]) {
		case 0:
			binarytree = new BinaryTree(20);
			break;
		case 1:
			findtree = new FindTree(20);
			binarytree = findtree;
			break;
		case 2:
			hufftree = new HuffTree();
			binarytree = hufftree;
		default:
			break;
		}
		Verbose("New tree assigned");

		DrawTree();
	}

	void WriteLog(System::String^ str) {
		logBox->AppendText(System::String::Concat(str, "\r\n"));
	}

public:
	void Info(System::String^ str) {
		if (!(logLevel & logLevels.info))
			return;
		WriteLog(System::String::Concat("[Info] ", str));
	}

	void Info(std::string str) {
		Info(gcnew System::String(str.c_str()));
	}

	void Verbose(System::String^ str) {
		if (!(logLevel & logLevels.verbose))
			return;
		WriteLog(System::String::Concat("[Verb] ", str));
	}

	void Error(System::String^ str) {
		if (!(logLevel & logLevels.error))
			return;
		WriteLog(System::String::Concat("[Erro] ", str));
	}

	bool CheckTreeLevel(BinaryTree* tree) {
		if (tree)
			return true;

		if (binarytree == nullptr) {
			Verbose("No tree was found");
			Info("Please build tree first");
		} else
			Info("Not implemented for current tree");

		return false;
	}

	System::String^ Ask(System::String^ message, bool digits) {
		AskDialog^ dialog = gcnew AskDialog(message, this, digits);
		Hide();
		dialog->ShowDialog();

		return dialog->value;
	}

	System::Void CleanDrawArea() {
		Graphics^ gr = this->drawPanel->CreateGraphics();
		gr->Clear(this->drawPanel->BackColor);
	}

	System::Void DrawTree() {
		if (!CheckTreeLevel(binarytree))
			return;

		CleanDrawArea();

		System::Drawing::Graphics^ gr = this->drawPanel->CreateGraphics();
		formtree->PrintT(gr, binarytree->getroot(), 0, drawPanel->Width - 26, 5, -1);
		Verbose("Tree drawn");
	}

private: System::Void drawToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void blackToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		//AssignTree(new BlTree(20));
	}
private: System::Void binaryToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		SetTree("binary");
	}
private: System::Void findToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		SetTree("find");
	}
	private: System::Void huffmanToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		SetTree("huff");
	}
private: System::Void findToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		System::String^ ask = Ask("Key", true);
		if (ask == "")
			return;

		uint32_t fkey = System::UInt32::Parse(ask);
		int fnode = binarytree->find(binarytree->getroot(), fkey);
		if (fnode != -1)
			Info(String::Concat("Found at level ", Convert::ToString(fnode)));
		else
			Info("No node with given key was found");
	}
private: System::Void heightToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		Info(String::Concat("Height is ", Convert::ToString(binarytree->height(binarytree->getroot()))));
	}
private: System::Void numberOfNodesToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		Info(String::Concat("Number of nodes is ", Convert::ToString(binarytree->nnode(binarytree->getroot()))));
	}
private: System::Void addToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		System::String^ fkey = Ask("Key", true);
		if (fkey == "")
			return;

		binarytree->add(binarytree->getroot(), System::Int32::Parse(fkey));
		Verbose(String::Concat("Added node with key ", fkey));
	}
private: System::Void byLevelToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		CleanDrawArea();
		System::Drawing::Graphics^ gr = this->drawPanel->CreateGraphics();
		formtree->PrintByLevel(gr, binarytree->getroot());
		Verbose("Tree drawn");
	}
private: System::Void wholeTreeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		DrawTree();
	}
private: System::Void idealToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		Info(String::Concat("The tree is ", binarytree->ideal(binarytree->getroot()) ? "" : "not", " ideal"));
	}
private: System::Void balanceToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		Info(String::Concat("The tree is ", binarytree->balance(binarytree->getroot()) ? "" : "not", " balanced"));
	}
private: System::Void sumToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		Info(String::Concat("Sum of all keys is ", System::Convert::ToString(binarytree->sum(binarytree->getroot()))));
	}
private: System::Void deleteToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(findtree))
			return;

		System::String^ ask = Ask("Enter key to delete", true);
		if (ask == "")
			return;

		uint32_t dkey = System::UInt32::Parse(ask);
		findtree->del(findtree->getroot(), dkey);
	}
private: System::Void replaceWithArrayToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(findtree))
			return;

		String^ ask = Ask("Enter array", false);
		if (ask == "")
			return;

		array<wchar_t>^ id = { ' ', ','  };
		array<String^>^ arr = ask->Split(id);

		int* intarr = new int[arr->Length];
		for (int i = 0; i < arr->Length; ++i)
			intarr[i] = System::UInt32::Parse(arr[i]);

		int i = 0;
		findtree->replace_with_array(findtree->getroot(), intarr, i);

		delete[] intarr;
	}
private: System::Void printEvenToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		int* arr = new int[20];
		int len = 0;

		binarytree->even_in_array(binarytree->getroot(), arr, len);

		System::String^ str = "";
		for (int i = 0; i < len; ++i)
			str += System::Convert::ToString(arr[i]) + " ";
		delete[] arr;

		Info(System::String::Concat("Even: ", str));
	}
private: System::Void minToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		int min;
		binarytree->min(binarytree->getroot(), min);
		Info(System::String::Concat("Min is ", System::Convert::ToString(min)));
	}
private: System::Void maxToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(binarytree))
			return;

		Info(System::String::Concat("Max is ", System::Convert::ToString(binarytree->max(binarytree->getroot()))));
	}
private: System::Void codeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(hufftree))
			return;

		if (openFileDialog->ShowDialog() != System::Windows::Forms::DialogResult::OK)
			return;

		//System::IO::StreamReader ^ sr = gcnew
		//	System::IO::StreamReader(openFileDialog->FileName);
		//MessageBox::Show(sr->ReadToEnd());
		//sr->Close();
		char* filename = and_SysStringToChar(openFileDialog->FileName);
		char* c_filename = and_SysStringToChar(System::IO::Path::ChangeExtension(openFileDialog->FileName, "_code.txt"));

		hufftree->periodicity(filename);
		hufftree->create_list();
		hufftree->create_tree();
		Info(hufftree->code(filename, c_filename));
	}
private: System::Void decodeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(hufftree))
			return;

		if (!hufftree->compression_ratio()) {
			Info("Please code file first");
			return;
		}

		if (openFileDialog->ShowDialog() != System::Windows::Forms::DialogResult::OK)
			return;

		char* filename = and_SysStringToChar(openFileDialog->FileName);
		Info(hufftree->decode(filename));
	}
private: System::Void compressionRateToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!CheckTreeLevel(hufftree))
			return;

		float ratio = hufftree->compression_ratio();
		if (!ratio) {
			Info("Bad ratio");
			return;
		}

		Info(System::String::Concat(System::Convert::ToString(ratio), "%"));
	}
};
}
